package clases;

/**
 * @author AyoubSaif
 * 
 */
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

import clases.Reserva;
import clases.ReservaPremium;
import clases.ReservaVIP;

public class GestorReservas {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Reserva> listaReservas;
	private ArrayList<ReservaPremium> listaReservasPremium;
	private ArrayList<ReservaVIP> listaReservasVIP;
	
	public GestorReservas() {
		listaClientes = new ArrayList<Cliente>();
		listaReservas = new ArrayList<Reserva>();
		listaReservasPremium = new ArrayList<ReservaPremium>();
		listaReservasVIP = new ArrayList<ReservaVIP>();
	}
	
	//metodos de cliente
	public void altaCliente(String dni, String nombre, String fechaNacimiento) {
		Cliente nuevoCliente = new Cliente(dni, nombre);
		nuevoCliente.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaClientes.add(nuevoCliente);		
	}
	
	public void listarClientes() {
		for (Cliente cliente:listaClientes) {
			if (cliente!=null) {
				System.out.println(cliente);
			}
		}
		
	}
	
	public Cliente buscarCliente (String dni) {
		for (Cliente cliente:listaClientes) {
			if (cliente!=null && cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}
	
	//metodos alta Reservas
	public void altaReserva(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan) {
		Reserva nuevaReserva = new Reserva(numero, nombreEjercicio, duracionEjercicio, precioPlan);
		listaReservas.add(nuevaReserva);
	}
	
	public void altaReservaPremium(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan) {
		ReservaPremium nuevaReservaPremium= new ReservaPremium(numero, nombreEjercicio, duracionEjercicio, precioPlan);
		listaReservasPremium.add(nuevaReservaPremium);
	}
	
	public void altaReservaVIP(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan, String[] invitados) {
		ReservaVIP nuevaReservaVIP= new ReservaVIP(numero, nombreEjercicio, duracionEjercicio, precioPlan, invitados);
		listaReservasVIP.add(nuevaReservaVIP);
	}
	
	public void listarReservas() {
		for (Reserva reserva : listaReservas) {
			if (reserva != null) {
				System.out.println(reserva);
			}
		}
	}

	public void listarReservasPremium() {
		for (ReservaPremium reserva : listaReservasPremium) {
			if (reserva != null) {
				System.out.println(reserva);
			}
		}
	}

	public void listarReservasVIP() {
		for (ReservaVIP reserva : listaReservasVIP) {
			if (reserva != null) {
				System.out.println(reserva);
			}
		}
	}
	
	public Reserva buscarReserva(int numero) {
		for (Reserva reserva : listaReservas) {
			if (reserva != null && reserva.getNumero() == numero) {
				return reserva;
			}
		}
		return null;
	}
	
	public ReservaPremium buscarReservaPremium(int numero) {
		for (ReservaPremium reserva : listaReservasPremium) {
			if (reserva != null && reserva.getNumero() == numero) {
				return reserva;
			}
		}
		return null;
	}

	public ReservaVIP buscarReservaVIP(int numero) {
		for (ReservaVIP reserva : listaReservasVIP) {
			if (reserva != null && reserva.getNumero() == numero) {
				return reserva;
			}
		}
		return null;
	}
	
	public void eliminarReserva(int numero) {
		Iterator<Reserva> iteradorReservas = listaReservas.iterator();
		while (iteradorReservas.hasNext()) {
			Reserva reserva = iteradorReservas.next();
			if (reserva.getNumero() == numero) {
				iteradorReservas.remove();
			}
		}
	}
	
	public void asignarReservaCliente(String dni, int numero) {
		Cliente cliente = buscarCliente(dni);
		Reserva reserva = buscarReserva(numero);
		reserva.setAsistente(cliente);
	}

	public void asignarReservaPremium(String dni, int numero) {
		Cliente cliente = buscarCliente(dni);
		ReservaPremium reserva = buscarReservaPremium(numero);
		reserva.setAsistente(cliente);
	}

	public void asignarReservaVIP(String dni, int numero) {
		Cliente cliente = buscarCliente(dni);
		ReservaVIP reserva = buscarReservaVIP(numero);
		reserva.setAsistente(cliente);
	}

	public void listarReservasDeAsistente(String dni) {
		for (Reserva reserva : listaReservas) {
			if (reserva.getAsistente() != null && reserva.getAsistente().getDni().equals(dni)) {
				System.out.println(reserva);
			}
		}
	}
}
