package clases;

/**
 * @author AyoubSaif
 * 
 */

public class Reserva {

	//SUPERCLASE
	/**
	 * Atributos
	 */
	int numero;
	String nombreEjercicio;
	int duracionEjercicio;
	Double precioPlan;
	Cliente asistente;
	
	/**
	 * Constructor
	 */

	public Reserva(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan) {
		this.numero = numero;
		this.nombreEjercicio = nombreEjercicio;
		this.duracionEjercicio = duracionEjercicio;
		this.precioPlan = precioPlan;
	}
	/**
	 * Getter y Setters
	 */
	
	
	public String getNombreEjercicio() {
		return nombreEjercicio;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public void setNombreEjercicio(String nombreEjercicio) {
		this.nombreEjercicio = nombreEjercicio;
	}
	public int getDuracionEjercicio() {
		return duracionEjercicio;
	}
	public void setDuracionEjercicio(int duracionEjercicio) {
		this.duracionEjercicio = duracionEjercicio;
	}

	public Double getPrecioPlan() {
		return precioPlan;
	}
	public void setPrecioPlan(Double precioPlan) {
		this.precioPlan = precioPlan;
	}
	
	public Cliente getAsistente() {
		return asistente;
	}
	public void setAsistente(Cliente asistente) {
		this.asistente = asistente;
	}
	
	/**
	 * toString
	 */
	
	@Override
	public String toString() {
		return "Ejercicio [nombreEjercicio=" + nombreEjercicio + ", duracionEjercicio=" + duracionEjercicio
				+ ", asistente=" + asistente + "]";
	}
}
