package clases;

/**
 * @author AyoubSaif
 * 
 */
import java.util.Arrays;

public class ReservaVIP extends Reserva{
	String[] Invitados;
	
	public ReservaVIP(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan, String[] invitados) {
		super(numero, nombreEjercicio, duracionEjercicio, precioPlan);
		this.Invitados = invitados;
	}

	public String[] getInvitados() {
		return Invitados;
	}

	public void setInvitados(String[] invitados) {
		Invitados = invitados;
	}

	/*
	 * ToString de ReservaVIP
	 * */

	@Override
	public String toString() {
		return "ReservaVIP [Invitados=" + Arrays.toString(Invitados) + ", numero=" + numero + ", nombreEjercicio="
				+ nombreEjercicio + ", duracionEjercicio=" + duracionEjercicio + ", precioPlan=" + precioPlan
				+ ", asistente=" + asistente + "]";
	}

	
}
