package clases;

/**
 * @author AyoubSaif
 * 
 */
public class ReservaPremium extends Reserva {

	/*
	 * Constructor de Reserva Premium
	 * */
	public ReservaPremium(int numero, String nombreEjercicio, int duracionEjercicio, Double precioPlan) {
		super(numero, nombreEjercicio, duracionEjercicio, precioPlan);
	}

	/*
	 * To String de ReservaPremium
	 * */
	
	@Override
	public String toString() {
		return "ReservaPremium [numero=" + numero + ", nombreEjercicio=" + nombreEjercicio + ", duracionEjercicio="
				+ duracionEjercicio + ", precioPlan=" + precioPlan + ", asistente=" + asistente + "]";
	}


}
